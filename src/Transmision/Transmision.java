/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transmision;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Random;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Transmision {
    private BufferedReader input;
    private PrintStream output;
    private JSONObject message;
    
    public Transmision(InputStream in, OutputStream out){
        this.input = new BufferedReader(new InputStreamReader(in));
        this.output = new PrintStream(out);
        this.message = new JSONObject();
        this.message.put("name", "");
        Random rnd = new Random();
        this.message.put("cod",(int)(rnd.nextDouble() * 100 + 0));
        this.message.put("data",new Object());
        this.message.put("status", 0);
    }
    
    public void putMessage(JSONObject obj){
        this.message = obj;
    }
    
    public void send(){
        this.output.print(this.message);
    }
    
    public void listen() throws IOException, ParseException, NullPointerException{
        try{
            String rq = this.input.readLine();
            System.out.println(rq);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(rq);
    //                JSONArray array = (JSONArray)obj;
            JSONObject obj1 = (JSONObject)obj;
            this.message.put("name",obj1.get("name"));
            this.message.put("cod",obj1.get("cod"));
            this.message.put("data",obj1.get("data"));
            this.message.put("status",obj1.get("status"));
        }catch(IOException ex){
            this.message.put("name","error");
            Random rnd = new Random();
            this.message.put("cod",(int)(rnd.nextDouble() * 100 + 0));
            this.message.put("data", "Error en la lectura del mensaje: \n"+ex.getMessage());
            this.message.put("status", -1);
        }catch(ParseException ex){
            this.message.put("name","error");
            Random rnd = new Random();
            this.message.put("cod",(int)(rnd.nextDouble() * 100 + 0));
            this.message.put("data", "Error en la decodificacion del mensaje: \n"+ex.getMessage());
            this.message.put("status", -1);
        }catch(NullPointerException ex){
            this.message.put("name","Un esclavo desconectado");
            Random rnd = new Random();
            this.message.put("cod",(int)(rnd.nextDouble() * 100 + 0));
            this.message.put("data", "Error en la decodificacion del mensaje: \n"+ex.getMessage());
            this.message.put("status", -1);
        }
    }
    
    public void llenarMessage(String name,String cod, Object ob, int s) {
        this.message = new JSONObject();
        this.message.put("name", name);
        this.message.put("cod",cod);
        this.message.put("data", ob);
        this.message.put("status",s);
    }

    public BufferedReader getInput() {
        return input;
    }

    public void setInput(BufferedReader input) {
        this.input = input;
    }

    public PrintStream getOutput() {
        return output;
    }

    public void setOutput(PrintStream output) {
        this.output = output;
    }

    public JSONObject getMessage() {
        return message;
    }

    public void setMessage(JSONObject message) {
        this.message = message;
    }
    
}
