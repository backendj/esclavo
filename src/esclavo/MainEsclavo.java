/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esclavo;

import Transmision.Transmision;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Random;
import org.json.simple.JSONObject;

public class MainEsclavo {
	
    /**
    * Puerto
    * */
    private final static int PORT = 5000;
    /**
    * Host
    * */
    private final static String SERVER = "localhost";
    
    public static void main(String[] args) {
        JSONObject obj = new JSONObject();
        obj.put("name", "Esclavo 1");
        Random rnd = new Random();
        obj.put("cod",(int)(rnd.nextDouble() * 100 + 0));
        obj.put("data","Conexion Exitosa");
        obj.put("status",1);
    	boolean exit=false;//bandera para controlar ciclo del programa
        Socket socket;//Socket para la comunicacion cliente servidor        
        try {            
            System.out.println("Cliente> Inicio");                               
            socket = new Socket(SERVER, PORT);//abre socket       
            Transmision tr = new Transmision(socket.getInputStream(),socket.getOutputStream());                          
            tr.putMessage(obj);
            tr.send();          
            socket.close(); 
            while( !exit ){//ciclo repetitivo  
                //Para leer lo que escriba el usuario            
                BufferedReader brRequest = new BufferedReader(new InputStreamReader(System.in));            
                System.out.println("Cliente:");
                System.out.println("1. Ingrese Muestras");
                System.out.println("2. Procese Muestras");
                System.out.println("3. Enviar Resultados");
                String request = brRequest.readLine();
                switch(request){
                    case "1":
                        System.out.println("Ingresando Muestras");
                        break;
                    case "2":
                        System.out.println("Procesando Muestras Muestras");
                        break;
                    case "3":{
                        socket = new Socket(SERVER, PORT);//abre socket
                        tr = new Transmision(socket.getInputStream(),socket.getOutputStream());
                        System.out.println("Enviando Muestras");
                        JSONObject obj1 = new JSONObject();
                        obj1.put("name", "Peticion");
                        obj1.put("cod",""+obj.get("cod"));
                        obj1.put("data",request);
                        obj1.put("status",1);
                        tr.putMessage(obj1);
                        tr.send();
                        //tr.llenarMessage("Peticion", ""+obj.get("cod"), request, 1);
                        System.out.println("Muestras Enviadas");
                        socket.close();
                    }
                        break;
                    default:
                        break;
                }
                if(request.equals("exit")){//terminar aplicacion
                    exit=true;                  
                    System.out.println("Cliente> Fin de programa");    
                }  
            }//end while                                    
       } catch (IOException ex) {        
         System.err.println("ERROR Cliente " + ex.getMessage());   
       }
    }
    
}
/**
 *
 * @author user
 */
